---
title: "Assessment"
date: 2017-11-27T09:11:38-07:00
draft: false
---

This is where the assessment instrument will be located. FSA's can use the instrument to rate their own sales attempts, and managers can use it to give feedback to FSA's.

The instrument will initially be a printable form, but can evolve into a form captured into a database - with results aggregated and progress measured for the FSA's growth. 
