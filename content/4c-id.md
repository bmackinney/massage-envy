+++
title = "Instructional Design"
description = "A description of the 4c-id model used in this training"
+++

I created this site to demonstrate my Instructional Design and work-related skills, as a part of the recruitment process for a position at Massage Envy Franchising as an [Instructional Designer](https://www.linkedin.com/jobs/view/instructional-designer-at-massage-envy-509687598).

## Task Prompt


## Instructional Design Model

[Four-Component Instructional Design](http://tensteps.info) is the most effective way to train people to perform complex skills.

## Learner

Massage Envy Franchise Sales Associates (FSA)

## Performance Context

In real life, this task takes place in the lobby of a Massage Envy franchise, and/or over the phone. It is performed in conversation, with the aid of marketing materials and the Point of Sale system.

## Learning Context

On a computer or smartphone, FSA's will read information and case studies, and watch videos of expert performance.

## Goal Analysis

1. Increase gift card Sales
  1. Increase FSA proactivity


## Four-Component Instructional Design


## Outcomes
- Business: Increase sales of gift cards.
- Sales Associates: Have more money
- Customers: Give and receive massage/facials as gifts.

## Competency
ME Sales Associates (MESAs)will sell gift cards to clients by proactively selling, building rapport, establishing needs, suggesting gift card options, overcoming objections, ringing up the gift card, and recording the encounter in the CRM system, using knowledge of gift card options available, .

## Tasks
Gift card sales encounters - a client interaction encompassing the entire time the MESA plans, attempts, and completes the gift card sale.

### Task Types
- Case Studies
- Completion: Given a client visit, plan, pre-chat, what will you say to suggest the sale? detect and overcome objections?
- Conventional: Knowing a client will come into the location soon, plan, chat, suggest, and complete the sale. This can be done in real life, and discussed in learning forum.

## Task Classes
Groups of equally-complex customer cases, ordered from simple to complex. Each class has all three of the task types in it.

1. Known, ready buyers during the holiday season, who have already purchased gift cards before.
1. Ready buyers during the holiday season.
1. Regular clients during the holiday season.
1. Reluctant clients during the holiday season.
1. Regular clients during dad/grad/wedding season.

Each following month, a new set of task classes can be published, relating to holidays and events in the month, birthdays, other occasions, relationship types.

.. N. New, reluctant clients in September.

## Assessment Instrument
### Objectives
1. MESAs will proactively attempt to sell gift cards

## Supportive Information - Proactive Selling
 - based on time of year
 - based on prior knowledge of the client

### Building Rapport

### Establishing Needs

### Suggestive Selling

### Overcoming Objections

## Procedural Information
### Ringing Up
1. Step 1

![picture of step1]()
1. Step 2

### Recording in CRM
1. Step 1

![picture of step1]()
1. Step 2

## Part-Task Practice
N/A - learners already expected to have mastered recurrent constituent skills. 

## Website
This site is built with [Hugo](https://www.gohugo.io), with source code hosted on [GitLab](https://about.gitlab.com), and built and deployed on [Netlify](https://www.netlify.com). Usage instructions are in the README.md file.
