---
title: "Example"
date: 2017-11-27T09:27:46-07:00
draft: false
weight: 1
---

This task will be a modeling example. All sales tasks should be a timeline of events as follows. Each can include a quoted dialogue or video embed.

## Review of Client Information in the CRM (if it exists)

This will be an image or embed of a PII-redacted client profile. In this case, the client will have previously purchased a gift card.


## Pre-Chat
A dialogue of the conversation between the expert FSA and the clients

Building rapport, establishing need, suggesting a product, (this class does not include objections).

### Dialogue Example
FSA:

> Hello! Welcome back!

Client:

> Thanks! I need this.

This video was embedded as an example - the real one would be a dialogue between customer and FSA.

{{% youtube Ajid9WdxffE %}}

## Closing the Sale

Client confirmation, ringing up, and packaging the product complete the example.

## Proactive Selling  
Link to the model information at [information/proactive-selling](/information/proactive-selling)
