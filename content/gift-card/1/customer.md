---
title: "Customer"
date: 2017-11-27T09:47:49-07:00
draft: false
weight: 3
---

The conventional task. This sale includes only the customer profile. The dialogue, suggestion, and sale completion process will be completed interactively, either with another FSA or with a real customer (TBD).
