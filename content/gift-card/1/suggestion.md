---
title: "Suggestion"
date: 2017-11-27T09:47:37-07:00
draft: false
weight: 2
---
This task includes the customer profile, an example dialogue in the pre-chat, and a prompt for the FSA to make the suggestion. 
